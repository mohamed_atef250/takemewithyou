package com.yumaas.takemewithyou.chat;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.models.Chat;

import java.util.ArrayList;
import java.util.HashMap;


public class UserChattersFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    HashMap<String,Boolean  >hashMap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chatters, container, false);


        menuList = rootView.findViewById(R.id.menu_list);

        hashMap = new HashMap<>();

        ArrayList<Chat> chatters = new ArrayList<>();

        ArrayList<Chat> chattersTemp = DataBaseHelper.getDataLists().chats;
        User user = DataBaseHelper.getSavedUser();


        for(int i=0; i<chattersTemp.size(); i++){

            if(user.id == chattersTemp.get(i).user.id){

                if(!hashMap.containsKey(chattersTemp.get(i).delegate.id+"")){
                    chatters.add(chattersTemp.get(i));
                }

                hashMap.put(chattersTemp.get(i).delegate.id+"",true);
            }

        }


        UserChattersAdapter chatAdapter = new UserChattersAdapter(getActivity(),chatters);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
