package com.yumaas.takemewithyou.chat;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.FragmentHelper;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.models.Chat;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class UserChattersAdapter extends RecyclerView.Adapter<UserChattersAdapter.ViewHolder> {

    Context context;
    ArrayList<Chat>chattersList;
    User temp;


    public UserChattersAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        temp = DataBaseHelper.getSavedUser();
        this.chattersList=chattersList;

    }


    @Override
    public UserChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        UserChattersAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserChattersAdapter.ViewHolder holder, final int position) {

        User user ;

        if(temp.type==0){
            user=chattersList.get(position).delegate;
        }else{
            user=chattersList.get(position).user;
        }


        holder.message.setText(user.username);


       ConnectionHelper.loadImage(holder.image,user.image);


        holder.itemView.setOnClickListener(view -> {

            UserChatFragment delegateChatFragment =   new UserChatFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("chatItem",chattersList.get(position));
            delegateChatFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), delegateChatFragment, "ChatFragment");
        });

    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
