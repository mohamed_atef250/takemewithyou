package com.yumaas.takemewithyou.chat;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.models.Chat;

import java.util.ArrayList;


public class UserChatFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;
    ArrayList<Chat> chatters, chats;
    EditText nameEditText;
    Button chatBtn;
    User reciver,user;
    UserChatAdapter userChatAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        menuList = rootView.findViewById(R.id.menu_list);

        nameEditText = rootView.findViewById(R.id.et_chat_message);
        chatBtn = rootView.findViewById(R.id.btn_chat);


        chats = new ArrayList<>();
        chatters = DataBaseHelper.getDataLists().chats;
        user = DataBaseHelper.getSavedUser();


        Chat chat = (Chat) getArguments().getSerializable("chatItem");


        reciver=chat.delegate;

        for (int i = 0; i < chatters.size(); i++) {

            if ((chatters.get(i).user.id == user.id &&
                    chatters.get(i).delegate.id == reciver.id) )
            chats.add(chatters.get(i));
        }
         userChatAdapter = new UserChatAdapter(getActivity(), chats);
        menuList.setAdapter(userChatAdapter);


        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Chat chat = new Chat(user,reciver,nameEditText.getText().toString());
                chat.sender=user.id;
                chats.add(chat);
                DataBaseHelper.addChat(chat);
                userChatAdapter.notifyDataSetChanged();
                nameEditText.setText("");
            }
        });

        return rootView;
    }


}
