package com.yumaas.takemewithyou.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.models.Chat;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class DelegateChatAdapter extends RecyclerView.Adapter<DelegateChatAdapter.ViewHolder> {

    private Context context;
    ArrayList<Chat> chattersList;

    User user;
    public DelegateChatAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList = chattersList;
        user = DataBaseHelper.getSavedUser();
    }


    @Override
    public DelegateChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        DelegateChatAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DelegateChatAdapter.ViewHolder holder, final int position) {


        if(chattersList.get(position).user !=null&&(user.id==chattersList.get(position).sender)) {
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ConnectionHelper.loadImage(holder.image,chattersList.get(position).delegate.image);

        }else {

            ConnectionHelper.loadImage(holder.image,chattersList.get(position).user.image);
            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }


        holder.message.setText(chattersList.get(position).message);
    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;
        public ViewHolder(View itemView) {
            super(itemView);

            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.message);
            image = itemView.findViewById(R.id.image);

        }
    }
}
