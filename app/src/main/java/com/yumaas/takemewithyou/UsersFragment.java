package com.yumaas.takemewithyou;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;

import java.util.ArrayList;

public class UsersFragment extends Fragment {

    ArrayList<User> users;
    View v;
    UsersAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);
        users = new ArrayList<>();
        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
            ArrayList<User>usersTemp = DataBaseHelper.getDataLists().users;
        for(int i=0; i<usersTemp.size(); i++){
            if(usersTemp.get(i).type==0)
            users.add(usersTemp.get(i));
        }

        citiesAdapter = new UsersAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        },users);
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}