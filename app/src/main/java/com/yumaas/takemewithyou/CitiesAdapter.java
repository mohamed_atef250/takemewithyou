package com.yumaas.takemewithyou;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.models.City;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    ArrayList<City>cities;

    public CitiesAdapter(OnItemClickListener onItemClickListener,ArrayList<City>cities) {
        this.onItemClickListener = onItemClickListener;
        this.cities=cities;
    }


    @Override
    public int getItemCount() {
        return cities.size();
    }


    @Override
    public CitiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        CitiesAdapter.ViewHolder viewHolder = new CitiesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final CitiesAdapter.ViewHolder holder, final int position) {
        holder.name.setText(cities.get(position).name);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد الحذف")
                        .setContentText("بالتاكيد حذف هذه المدينه ؟")
                        .setConfirmText("نعم احذف").setConfirmClickListener(sweetAlertDialog -> {
                    DataBaseHelper.removeAd(cities.get(position));
                    cities.remove(position);

                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }).show();
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        Button delete,showMap;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tv_review_reviewer);
            delete=view.findViewById(R.id.delete);
            showMap=view.findViewById(R.id.showMap);

        }
    }
}