package com.yumaas.takemewithyou;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.sucho.placepicker.AddressData;
import com.sucho.placepicker.Constants;
import com.sucho.placepicker.PlacePicker;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.Validate;
import com.yumaas.takemewithyou.models.City;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddCityFragment extends Fragment {

    View v;
    EditText location,name;
    double lat,lng;
    Button btn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_city, container, false);
        name=v.findViewById(R.id.name);
        location=v.findViewById(R.id.location);
        btn = v.findViewById(R.id.btn);

        location.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(),SelectLocationActivity.class);
            startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST);
        });

        btn.setOnClickListener(view -> {

            if(Validate.isEmpty(name.getText().toString())||lat==0.0||lng==0.0){
                SweetDialogs.errorMessage(getActivity(),"اكمل البيانات من فضلك");
                return;
            }
            DataBaseHelper.addCities(new City(name.getText().toString(),lat,lng));

            SweetDialogs.successMessage(getActivity(), "تم اضافه المدينه", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    FragmentHelper.popAllFragments(getActivity());
                    FragmentHelper.addFragment(getActivity(),new CitiesFragments(),"CitiesFragments");
                }
            });
        });

        return v;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                location.setText("تم اختيار الموقع بنجاح");

                lat=data.getDoubleExtra("lat",0.0);
                lng=data.getDoubleExtra("lng",0.0);
             }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }





}