package com.yumaas.takemewithyou;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.base.Validate;
import com.yumaas.takemewithyou.base.filesutils.FileOperations;
import com.yumaas.takemewithyou.base.filesutils.VolleyFileObject;
import com.yumaas.takemewithyou.databinding.FragmentRegisterBinding;
import com.yumaas.takemewithyou.delegate.DelegateMainActivity;
import com.yumaas.takemewithyou.user.UserMainActivity;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;
import com.yumaas.takemewithyou.volleyutils.ConnectionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class RegisterActivity extends AppCompatActivity {

    FragmentRegisterBinding fragmentRegisterBinding;
    int type=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentRegisterBinding = DataBindingUtil.setContentView(this, R.layout.fragment_register);

       fragmentRegisterBinding.radioGroup.setOnCheckedChangeListener((radioGroup, i) -> {

           if(i==R.id.radioButton){
               type=0;
           }else {

               type=1;
           }


       });

        fragmentRegisterBinding.register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(fragmentRegisterBinding.etRegisterName.getText().toString(),
                        fragmentRegisterBinding.etRegisterEmail.getText().toString(),
                        fragmentRegisterBinding.etRegisterPasswords.getText().toString(),
                        fragmentRegisterBinding.etRegisterPhone.getText().toString());
                user.image=selectedImage;
                user.type=type;
                DataBaseHelper.addUser(user);
                DataBaseHelper.saveStudent(user);
                SweetDialogs.successMessage(RegisterActivity.this, "تم التسجيل بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent(RegisterActivity.this, UserMainActivity.class);
                        if(user.type==1){
                            intent = new Intent(RegisterActivity.this, DelegateMainActivity.class);
                        }
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });

        fragmentRegisterBinding.nationalId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });
    }




    private boolean validate(){

        if(Validate.isEmpty(fragmentRegisterBinding.etRegisterName.getText().toString())){
            fragmentRegisterBinding.etRegisterName.setError("ادخل الاسم");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.etRegisterEmail.getText().toString())){
            fragmentRegisterBinding.etRegisterEmail.setError("ادخل البريد الالكتروني");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.etRegisterPhone.getText().toString())){
            fragmentRegisterBinding.etRegisterPhone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.etRegisterPasswords.getText().toString())){
            fragmentRegisterBinding.etRegisterPasswords.setError("ادخل كلمه المرور");
            return false;
        }else if(!Validate.isMail(fragmentRegisterBinding.etRegisterEmail.getText().toString())){
            fragmentRegisterBinding.etRegisterEmail.setError("البريد الالكتروني خاطئ");
            return false;
        }

        return true;
    }

    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(RegisterActivity.this, data, "image",
                            43);


                fragmentRegisterBinding.nationalId.setText("تم اضافه الهويه بنجاح");

            volleyFileObjects.add(volleyFileObject);



            addServiceApi();


    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                    selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }




}
