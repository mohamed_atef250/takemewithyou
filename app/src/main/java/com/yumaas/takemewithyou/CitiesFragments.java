package com.yumaas.takemewithyou;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.base.DataBaseHelper;

public class CitiesFragments extends Fragment {


    View v;
    CitiesAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        citiesAdapter = new CitiesAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, DataBaseHelper.getDataLists().cities);
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}