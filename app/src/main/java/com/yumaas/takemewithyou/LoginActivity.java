package com.yumaas.takemewithyou;


import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.databinding.FragmentLoginBinding;
import com.yumaas.takemewithyou.delegate.DelegateMainActivity;
import com.yumaas.takemewithyou.user.UserMainActivity;


public class LoginActivity extends AppCompatActivity {

    FragmentLoginBinding
            fragmentLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, 342);
        }

        fragmentLoginBinding = DataBindingUtil.setContentView(this, R.layout.fragment_login);

        fragmentLoginBinding.btnLoginSignIn.setOnClickListener(view -> {
            if((fragmentLoginBinding.email.getText().toString().equals("admin"))
                    &&fragmentLoginBinding.password.getText().toString().equals("123456")){
                Intent intent = new Intent(LoginActivity.this, AdminMainPage.class);
                startActivity(intent);
            }else{
                User user = DataBaseHelper.loginUser(fragmentLoginBinding.email.getText().toString()
                        , fragmentLoginBinding.password.getText().toString());
                if (user != null) {
                    DataBaseHelper.saveStudent(user);
                    Intent intent = new Intent(LoginActivity.this, UserMainActivity.class);
                    if(user.type==1){
                        intent = new Intent(LoginActivity.this, DelegateMainActivity.class);
                    }
                    startActivity(intent);
                    finish();
                } else {
                    SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                }
            }
        });


        fragmentLoginBinding.tvLoginRegister.setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));


    }


}
