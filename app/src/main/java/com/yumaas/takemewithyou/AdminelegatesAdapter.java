package com.yumaas.takemewithyou;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;


public class AdminelegatesAdapter extends RecyclerView.Adapter<AdminelegatesAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;



    public AdminelegatesAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;


    }


    @Override
    public int getItemCount() {
        return 10;
    }


    @Override
    public AdminelegatesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_delegate, parent, false);
        AdminelegatesAdapter.ViewHolder viewHolder = new AdminelegatesAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AdminelegatesAdapter.ViewHolder holder, final int position) {


    }


    static class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View view) {
            super(view);



        }
    }
}