package com.yumaas.takemewithyou;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;

public class AdminMainPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 134);
        }


        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);



        View view = navigationView.getHeaderView(0);
        ImageView imageView = view.findViewById(R.id.imageView);
        TextView username=view.findViewById(R.id.username);
        TextView phone=view.findViewById(R.id.phone);

        ConnectionHelper.loadImage(imageView,"https://jonathonschuster.com/static/images/user-placeholder.png");
        username.setText("مسئول التطبيق");
        phone.setText("");

        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    drawer.closeDrawers();
                    if (menuItem.getItemId() == R.id.nav_home) {
                        FragmentHelper.addFragment(AdminMainPage.this, new DelegatesFragment(), "DelegatesFragment");
                    }else if(menuItem.getItemId() == R.id.nav_show_cities) {
                        FragmentHelper.addFragment(AdminMainPage.this, new CitiesFragments(), "CitiesFragments");
                    }else if(menuItem.getItemId() == R.id.nav_add_city) {
                        FragmentHelper.addFragment(AdminMainPage.this, new AddCityFragment(), "AddCityFragment");
                    }else if (menuItem.getItemId() == R.id.nav_show_users) {
                        FragmentHelper.addFragment(AdminMainPage.this, new UsersFragment(), "UsersFragment");
                    }else {
                        finish();
                    }

                    return false;
                });

        FragmentHelper.addFragment(this, new DelegatesFragment(), "DelegatesFragment");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}