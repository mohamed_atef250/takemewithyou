package com.yumaas.takemewithyou.delegate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.OnItemClickListener;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.models.Ads;

import java.util.ArrayList;

public class MyAdsFragment extends Fragment {


    View v;
    MyAdsAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        ArrayList<Ads>ads = new ArrayList<>();
        ArrayList<Ads>adsTemp = DataBaseHelper.getDataLists().ads;

        User user = DataBaseHelper.getSavedUser();
        for(int i=0; i<adsTemp.size(); i++){
            if(user.id==adsTemp.get(i).userId)
            ads.add(adsTemp.get(i));
        }

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        citiesAdapter = new MyAdsAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        },ads);
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}