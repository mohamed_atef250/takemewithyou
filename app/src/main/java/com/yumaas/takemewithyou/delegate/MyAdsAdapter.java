package com.yumaas.takemewithyou.delegate;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.OnItemClickListener;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.models.Ads;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class MyAdsAdapter extends RecyclerView.Adapter<MyAdsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Ads>ads;


    public MyAdsAdapter(OnItemClickListener onItemClickListener,ArrayList<Ads>ads) {
        this.onItemClickListener = onItemClickListener;
        this.ads=ads;
    }


    @Override
    public int getItemCount() {
        return ads.size();
    }


    @Override
    public MyAdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_ads, parent, false);
        MyAdsAdapter.ViewHolder viewHolder = new MyAdsAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final MyAdsAdapter.ViewHolder holder, final int position) {

        holder.fromCity.setText(" ارغب في توصيل طلب من مدينه "+ads.get(position).fromCity+"  لمدينه "
        +ads.get(position).toCity);

        holder.toCity.setText(ads.get(position).details
        );

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد الحذف")
                        .setContentText("بالتاكيد حذف هذا الاعلان ؟")
                        .setConfirmText("نعم احذف").setConfirmClickListener(sweetAlertDialog -> {
                    DataBaseHelper.removeAd(ads.get(position));
                    ads.remove(position);

                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }).show();
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView fromCity,toCity;
        Button delete;

        public ViewHolder(View view) {
            super(view);
            fromCity = view.findViewById(R.id.fromCity);
            toCity = view.findViewById(R.id.toCity);
            delete = view.findViewById(R.id.delete);
        }
    }
}