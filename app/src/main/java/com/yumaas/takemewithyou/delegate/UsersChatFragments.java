package com.yumaas.takemewithyou.delegate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.OnItemClickListener;
import com.yumaas.takemewithyou.R;

public class UsersChatFragments extends Fragment {


    View v;
    UsersChatAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        citiesAdapter = new UsersChatAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        });
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}