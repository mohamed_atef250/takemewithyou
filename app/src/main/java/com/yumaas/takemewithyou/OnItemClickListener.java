package com.yumaas.takemewithyou;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
