package com.yumaas.takemewithyou.base;




import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import com.yumaas.takemewithyou.models.Ads;
import com.yumaas.takemewithyou.models.Chat;
import com.yumaas.takemewithyou.models.City;
import com.yumaas.takemewithyou.models.Order;
import com.yumaas.takemewithyou.volleyutils.MyApplication;


import java.util.ArrayList;



public class DataBaseHelper {

    private static SharedPreferences sharedPreferences = null;


    private DataBaseHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance() {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }


    public static void addUser(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users == null) {
            users = new ArrayList<>();
        }
        users.add(student);
        dataLists.users = users;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }



    public static User loginUser(String email,String password) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                Log.e("LOGS",dataLists.users.get(i).email+" "+dataLists.users.get(i).password);
                if (dataLists.users.get(i).email.equals(email)&&
                        dataLists.users.get(i).password.equals(password)) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }

    public static User findUser(int id) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id == id ) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }


    public static void updateUser(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==student.id) {
                    dataLists.users.set(i,student);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void removeUser(User helper) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            dataLists.users = users;
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id==helper.id) {
                    dataLists.users.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }


    public static int generateId() {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        int id = getSharedPreferenceInstance().getInt("id",1);
        id++;
        prefsEditor.putInt("id", id);
        prefsEditor.apply();

        return id;
    }



    public static DataLists getDataLists() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("users", "");
        if (json.equals("")) return new DataLists();
        return gson.fromJson(json, DataLists.class);
    }


    public static User getSavedUser() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance().getString("savedUser", "");
        return gson.fromJson(json, User.class);
    }

    public static void saveStudent(User student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(student);
        prefsEditor.putString("savedUser", json);
        prefsEditor.apply();
    }







    public static void addAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads == null) {
            ads = new ArrayList<>();
        }
        ads.add(ad);
        dataLists.ads = ads;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void updateAds(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ads != null) {

            dataLists.ads = ads;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.ads.get(i).id==ad.id) {
                    dataLists.ads.set(i,ad);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void removeAd(Ads ad) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Ads> ads = dataLists.ads;
        if (ad != null) {
            dataLists.ads = ads;
            for (int i = 0; i < dataLists.ads.size(); i++) {
                if (dataLists.ads.get(i).id==ad.id) {
                    dataLists.ads.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void addCities(City city) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<City> cities = dataLists.cities;
        if (cities == null) {
            cities = new ArrayList<>();
        }
        cities.add(city);
        dataLists.cities = cities;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }

    public static void addChat(Chat chat) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Chat> chats = dataLists.chats;
        if (chats == null) {
            chats = new ArrayList<>();
        }
        chats.add(chat);
        dataLists.chats = chats;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }


    public static void updateAds(City city) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<City> cities = dataLists.cities;
        if (cities != null) {

            dataLists.cities = cities;

            for (int i = 0; i < dataLists.cities.size(); i++) {
                if (dataLists.cities.get(i).id== city.id) {
                    dataLists.cities.set(i, city);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }



    public static void removeAd(City city) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<City> cities = dataLists.cities;
        if (city != null) {
            dataLists.cities = cities;
            for (int i = 0; i < dataLists.cities.size(); i++) {
                if (dataLists.cities.get(i).id== city.id) {
                    dataLists.cities.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("users", json);
            prefsEditor.apply();
        }
    }


    public static void addOrder(Order order) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance().edit();
        DataLists dataLists = getDataLists();
        ArrayList<Order> orders = dataLists.orders;
        if (orders == null) {
            orders = new ArrayList<>();
        }
        orders.add(order);
        dataLists.orders = orders;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("users", json);
        prefsEditor.apply();
    }




}
