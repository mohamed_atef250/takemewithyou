package com.yumaas.takemewithyou.base;


import com.yumaas.takemewithyou.models.Ads;
import com.yumaas.takemewithyou.models.Chat;
import com.yumaas.takemewithyou.models.City;
import com.yumaas.takemewithyou.models.Order;

import java.util.ArrayList;

public class DataLists {
    public ArrayList<User> users = new ArrayList<>();
    public ArrayList<Ads> ads = new ArrayList<>();
    public ArrayList<City> cities = new ArrayList<>();
    public ArrayList<Order> orders = new ArrayList<>();
    public ArrayList<Chat>  chats = new ArrayList<>();

}
