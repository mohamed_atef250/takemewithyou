package com.yumaas.takemewithyou.base;

import android.graphics.Bitmap;

import com.github.bassaer.chatmessageview.model.IChatUser;

import java.io.Serializable;

public class User   implements Serializable {
   public int id;
    public String username;
    public String email,password,phone,image;
    public float rating=0f;

    public int type=0;


    public User(String username, String email, String password, String phone) {
        this.id=DataBaseHelper.generateId();
        this.username = username;
        this.password=password;
        this.phone=phone;
        this.email = email;
    }





}
