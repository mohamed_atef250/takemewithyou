package com.yumaas.takemewithyou;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;



public class MainActivity extends AppCompatActivity {
    EditText text;
    Button search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FragmentHelper.addFragment(this,new HomeFragment(),"HomeFragment");
    }

    boolean doubleBackToExitPressedOnce = true;

    @Override
    public void onBackPressed() {


        if (!doubleBackToExitPressedOnce) {
            finish();
        } else {

            this.doubleBackToExitPressedOnce = false;
            Toast.makeText(this, "اشغط مرتين للخروج", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = true, 2000);
          }

    }
}