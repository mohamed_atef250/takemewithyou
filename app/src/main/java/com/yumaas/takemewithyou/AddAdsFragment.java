package com.yumaas.takemewithyou;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.sucho.placepicker.Constants;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.delegate.MyAdsFragment;
import com.yumaas.takemewithyou.models.Ads;
import com.yumaas.takemewithyou.models.City;

import java.lang.reflect.Array;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddAdsFragment extends Fragment {

    View v;
    EditText from_city,from_city_location,to_city,to_city_location,details;
    int city1=0,city2=0;
    boolean isFirst=false;
    Button add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_ads, container, false);
        from_city=v.findViewById(R.id.from_city);
        from_city_location=v.findViewById(R.id.from_city_location);
        to_city=v.findViewById(R.id.to_city);
        to_city_location=v.findViewById(R.id.to_city_location);
        details=v.findViewById(R.id.details);
        add=v.findViewById(R.id.add);

        from_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFirst=true;
                addCity(from_city);
            }
        });

        to_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFirst=false;
                addCity(to_city);
            }
        });


        from_city_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFirst=true;
                Intent intent = new Intent(getActivity(),SelectLocationActivity.class);
                startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST);
            }
        });

        to_city_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFirst=false;
                Intent intent = new Intent(getActivity(),SelectLocationActivity.class);
                startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataBaseHelper.addAds(new Ads(details.getText().toString(),cities.get(city1).name
                        ,cities.get(city2).name
                        ,lat,lng,lat2,lng2));

                SweetDialogs.successMessage(getActivity(), "تم اضافه الاعلان", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        FragmentHelper.popAllFragments(getActivity());
                        FragmentHelper.addFragment(getActivity(),new MyAdsFragment(),"MyAdsFragment");
                    }
                });
            }
        });

        return v;
    }

    ArrayList<City> cities;

    private void addCity(EditText editText){

        PopupMenu popup = new PopupMenu(getActivity(), editText);

       cities = DataBaseHelper.getDataLists().cities;

        for(int i=0; i<cities.size(); i++){
            popup.getMenu().add(i,i,i,""+cities.get(i).name);
        }


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(isFirst){
                    city1=item.getItemId();
                }else {
                    city2 = item.getItemId();
                }
                editText.setText(item.getTitle());
                return true;
            }
        });

        popup.show();
    }


    double lat=0.0,lat2=0.0,lng=0.0,lng2=0.0;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == Constants.PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {

                if(isFirst){
                    from_city_location.setText("تم اختيار الموقع بنجاح");

                    lat=data.getDoubleExtra("lat",0.0);
                    lng=data.getDoubleExtra("lng",0.0);
                }else {
                    to_city_location.setText("تم اختيار الموقع بنجاح");

                    lat2=data.getDoubleExtra("lat",0.0);
                    lng2=data.getDoubleExtra("lng",0.0);
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }





}