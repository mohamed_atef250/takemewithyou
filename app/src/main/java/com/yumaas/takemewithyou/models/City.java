package com.yumaas.takemewithyou.models;


import com.yumaas.takemewithyou.base.DataBaseHelper;

import java.io.Serializable;

public class City implements Serializable {

    public int id;
    public String name;
    public double lat,lng;

    public City(String name, double lat,double lng){
        this.id= DataBaseHelper.generateId();
        this.name=name;
        this.lat=lat;
        this.lng=lng;

    }
}
