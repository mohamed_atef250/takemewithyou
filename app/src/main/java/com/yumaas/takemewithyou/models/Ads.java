package com.yumaas.takemewithyou.models;


import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;

public class Ads {
    public int id;
    public int userId;
    public User user;
    public String details;
    public String fromCity,toCity;
    public double fromLat,toLat,fromLng,toLng;


    public Ads(String details,String fromCity,String toCity,
               double fromLat,double toLat,double fromLng,double toLng){
        this.id= DataBaseHelper.generateId();
        this.user = DataBaseHelper.getSavedUser();
        this.details=details;
        this.fromCity=fromCity;
        this.toCity=toCity;
        this.fromLat=fromLat;
        this.toLat=toLat;
        this.fromLng=fromLng;
        this.toLng=toLng;
        this.userId=DataBaseHelper.getSavedUser().id;

    }

}
