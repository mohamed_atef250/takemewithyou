package com.yumaas.takemewithyou.models;



import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;

import java.io.Serializable;

public class Order implements Serializable {

    public int id,quantity=0;
    public User user;
    public City city;
    public Order(User user, City city, int quantity){
        this.id= DataBaseHelper.generateId();
        this.user=user;
        this.city = city;
        this.quantity=quantity;
    }
}
