package com.yumaas.takemewithyou.models;

import com.yumaas.takemewithyou.base.User;

import java.io.Serializable;

public class Chat implements Serializable {

    public User delegate;
    public User user;
    public int sender=0;
    public String message;

    public Chat(User user, User delegate, String message){
        this.user=user;
        this.delegate=delegate;
        this.message=message;
    }

}
