package com.yumaas.takemewithyou.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.FragmentHelper;
import com.yumaas.takemewithyou.OnItemClickListener;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.chat.DelegateChatFragment;
import com.yumaas.takemewithyou.models.Chat;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<User> users;



    public SearchAdapter(OnItemClickListener onItemClickListener, ArrayList<User>users) {
        this.onItemClickListener = onItemClickListener;
        this.users=users;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
        SearchAdapter.ViewHolder viewHolder = new SearchAdapter.ViewHolder(view);


        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final SearchAdapter.ViewHolder holder, final int position) {

        holder.name.setText(users.get(position).username);
        ConnectionHelper.loadImage(holder.image, users.get(position).image);
        holder.ratingbar.setRating(users.get(position).rating);

        holder.chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DelegateChatFragment delegateChatFragment = new DelegateChatFragment();
                Bundle bundle = new Bundle();
                Chat chat = new Chat(DataBaseHelper.getSavedUser(),users.get(position), "");
                bundle.putSerializable("chatItem", chat);
                delegateChatFragment.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(), delegateChatFragment, "ChatFragment");

            }
        });



    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        Button  chat;
        TextView name;
        ImageView image;
        MaterialRatingBar ratingbar;

        public ViewHolder(View view) {
            super(view);

            chat = view.findViewById(R.id.chat);
            name = view.findViewById(R.id.tv_review_distance);
            image = view.findViewById(R.id.image);
            ratingbar = view.findViewById(R.id.ratingbar);

        }
    }
}