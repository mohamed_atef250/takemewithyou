package com.yumaas.takemewithyou.user;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.yumaas.takemewithyou.AddAdsFragment;
import com.yumaas.takemewithyou.FragmentHelper;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.chat.UserChattersFragment;
import com.yumaas.takemewithyou.delegate.MyAdsFragment;
import com.yumaas.takemewithyou.delegate.UsersChatFragments;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;

public class UserMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 134);
        }


        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        View view = navigationView.getHeaderView(0);
        ImageView imageView = view.findViewById(R.id.imageView);
        TextView username=view.findViewById(R.id.username);
        TextView phone=view.findViewById(R.id.phone);
        User user = DataBaseHelper.getSavedUser();
        ConnectionHelper.loadImage(imageView,user.image);
        username.setText(user.username);
        phone.setText(user.phone);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                drawer.closeDrawers();
                if (menuItem.getItemId() == R.id.nav_home) {
                    FragmentHelper.addFragment(UserMainActivity.this, new DelegateAdsFragment(), "DelegatesFragment");
                }
                else  if (menuItem.getItemId() == R.id.my_ads) {
                    FragmentHelper.addFragment(UserMainActivity.this, new MyAdsFragment(), "DelegatesFragment");
                }else if (menuItem.getItemId() == R.id.nav_show_users) {
                    FragmentHelper.addFragment(UserMainActivity.this, new UserChattersFragment(), "UsersFragment");
                }else if (menuItem.getItemId() == R.id.nav_show_users) {
                    FragmentHelper.addFragment(UserMainActivity.this, new UserChattersFragment(), "UsersFragment");
                }
                else if (menuItem.getItemId() == R.id.nav_add_ads) {
                    FragmentHelper.addFragment(UserMainActivity.this, new AddAdsFragment(), "UsersFragment");
                }else {
                    finish();
                }
                return false;
            }
        });

        FragmentHelper.addFragment(this, new DelegateAdsFragment(), "MyAdsFragment");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}