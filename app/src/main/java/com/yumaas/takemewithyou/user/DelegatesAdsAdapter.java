package com.yumaas.takemewithyou.user;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.FragmentHelper;
import com.yumaas.takemewithyou.OnItemClickListener;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.chat.DelegateChatFragment;
import com.yumaas.takemewithyou.chat.UserChatFragment;
import com.yumaas.takemewithyou.delegate.MessengerActivity;
import com.yumaas.takemewithyou.models.Ads;
import com.yumaas.takemewithyou.models.Chat;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class DelegatesAdsAdapter extends RecyclerView.Adapter<DelegatesAdsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Ads> ads;

    User user;

    public DelegatesAdsAdapter(OnItemClickListener onItemClickListener, ArrayList<Ads> ads) {
        this.onItemClickListener = onItemClickListener;
        this.ads = ads;
        user = DataBaseHelper.getSavedUser();
    }


    @Override
    public int getItemCount() {
        return ads.size();
    }


    @Override
    public DelegatesAdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_delegate_ad, parent, false);
        DelegatesAdsAdapter.ViewHolder viewHolder = new DelegatesAdsAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final DelegatesAdsAdapter.ViewHolder holder, final int position) {

        holder.name.setText(ads.get(position).user.username);

        ConnectionHelper.loadImage(holder.image, ads.get(position).user.image);

        holder.from_city.setText(" من مدينه " + ads.get(position).fromCity);
        holder.to_city.setText(" الي مدينه " + ads.get(position).toCity);
        holder.ratingbar.setRating(ads.get(position).user.rating);
        holder.chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserChatFragment delegateChatFragment = new UserChatFragment();
                Bundle bundle = new Bundle();
                Chat chat = new Chat(DataBaseHelper.getSavedUser(), ads.get(position).user, "");
                bundle.putSerializable("chatItem", chat);
                delegateChatFragment.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(), delegateChatFragment, "ChatFragment");

            }
        });

        if(user.type==1){

            holder.rate.setVisibility(View.GONE);
        }else {
            holder.rate.setVisibility(View.VISIBLE);
        }

        holder.rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rating(view.getContext(), 0f, ads.get(position
                ).user.username, position);
            }
        });

    }

    private void rating(Context context, float userRankValue, String name, int pos) {
        Dialog rankDialog = new Dialog(context);
        rankDialog.setContentView(R.layout.rank_dialog);
        rankDialog.setCancelable(true);
        RatingBar ratingBar = (RatingBar) rankDialog.findViewById(R.id.dialog_ratingbar);
        ratingBar.setRating(userRankValue);

        TextView text = (TextView) rankDialog.findViewById(R.id.rank_dialog_text1);
        text.setText(name);

        Button updateButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ads.get(pos).user.rating = ratingBar.getRating();
                notifyDataSetChanged();
                rankDialog.dismiss();
            }
        });
        rankDialog.show();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        Button rate, chat;
        TextView name, from_city, to_city;
        ImageView image;
        MaterialRatingBar ratingbar;

        public ViewHolder(View view) {
            super(view);
            rate = view.findViewById(R.id.rate);
            chat = view.findViewById(R.id.chat);
            name = view.findViewById(R.id.name);
            from_city = view.findViewById(R.id.from_city);
            to_city = view.findViewById(R.id.to_city);
            image = view.findViewById(R.id.image);
            ratingbar = view.findViewById(R.id.ratingbar);

        }
    }
}