package com.yumaas.takemewithyou.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.OnItemClickListener;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.delegate.MyAdsAdapter;
import com.yumaas.takemewithyou.models.Ads;

import java.util.ArrayList;

public class SearchFragment extends Fragment {


    View v;
    SearchAdapter citiesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);

        ArrayList<User>users = new ArrayList<>();
        ArrayList<User>usersTemp = DataBaseHelper.getDataLists().users;

       String querry=getArguments().getString("query");

        for(int i=0; i<usersTemp.size(); i++){
            if(usersTemp.get(i).username.contains(querry))
                users.add(usersTemp.get(i));
        }

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        citiesAdapter = new SearchAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        },users);
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}