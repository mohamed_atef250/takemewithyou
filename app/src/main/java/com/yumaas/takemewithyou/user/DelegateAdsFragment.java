package com.yumaas.takemewithyou.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.FragmentHelper;
import com.yumaas.takemewithyou.OnItemClickListener;
import com.yumaas.takemewithyou.R;
import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.models.Ads;

import java.util.ArrayList;

public class DelegateAdsFragment extends Fragment {


    View v;
    DelegatesAdsAdapter citiesAdapter;
    SearchView seaarchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_companies, container, false);
        ArrayList<Ads>ads = new ArrayList<>();
        ArrayList<Ads>adsTemp = DataBaseHelper.getDataLists().ads;
        seaarchView=v.findViewById(R.id.seaarchView);


        User user = DataBaseHelper.getSavedUser();

        if(user.type==0){
            seaarchView.setVisibility(View.VISIBLE);
        }

        seaarchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Bundle bundle =new  Bundle();
                bundle.putString("query",seaarchView.getQuery().toString());
                SearchFragment searchFragment =    new SearchFragment();
                searchFragment.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(),searchFragment,"SearchFragment");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        seaarchView.setOnSearchClickListener(view -> {


        });


        for(int i=0; i<adsTemp.size(); i++){
            if(user.id!=adsTemp.get(i).userId)
                ads.add(adsTemp.get(i));
        }


        RecyclerView recyclerView = v.findViewById(R.id.recyclerview);
        citiesAdapter = new DelegatesAdsAdapter(new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, ads);
        recyclerView.setAdapter(citiesAdapter);
        return v;
    }





}