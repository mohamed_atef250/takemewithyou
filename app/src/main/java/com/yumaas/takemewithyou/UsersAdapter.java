package com.yumaas.takemewithyou;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.takemewithyou.base.DataBaseHelper;
import com.yumaas.takemewithyou.base.User;
import com.yumaas.takemewithyou.volleyutils.ConnectionHelper;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<User>users;


    public UsersAdapter(OnItemClickListener onItemClickListener, ArrayList<User>users) {
        this.onItemClickListener = onItemClickListener;
        this.users=users;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_users, parent, false);
        UsersAdapter.ViewHolder viewHolder = new UsersAdapter.ViewHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UsersAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.image,users.get(position).image);
        holder.name.setText(users.get(position).username);
        holder.phone.setText(users.get(position).phone);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("هل تريد الحذف")
                        .setContentText("بالتاكيد حذف هذا الطفل ؟")
                        .setConfirmText("نعم احذف").setConfirmClickListener(sweetAlertDialog -> {
                    DataBaseHelper.removeUser(users.get(position));
                    users.remove(position);

                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }).show();
            }
        });


    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name,phone;
        Button delete;

        public ViewHolder(View view) {
            super(view);

            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            phone = view.findViewById(R.id.phone);
            delete=view.findViewById(R.id.delete);
        }
    }
}